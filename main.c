/*
Nick Walton's Maze Generator
Generates a maze with my custom "Persistent Walk" algorithm, which essentially varies in the type of mazes generated from Prim's to Recursive Backtracker based on the persistence value given.

Generates a maze, drawing at each change to the maze. Then uses Dijkstra's to calculate the "weight" of each point - its distance from the starting point - and draws a colourized maze with the path from start->finish in grey.

ESCAPE to exit at any time.
SPACE during maze generation stage to skip drawing.
SPACE after maze generation is complete to switch between colourized maze and plain maze.
G after genration is complete to generate new maze.

To change the type of maze generated, search for the "MazeGenerate[...]" function. The arguments are:
*maze, *walls, num_cells_x, num_cells_y, cell_size (square), persistence(floating point), *frame_buffer

Persistence = 0 generates mazes with very short corridors, constant forking, radial texture. Very similar to Prim's.
Persistence = infinity generates mazes with very long corridors, disarrayed texture. Very similar to Recursive Backtracker.



main.h created by Nicholas Walton. Released to the public domain. If a license is required, licensed under MIT.

*******************************************************************************
*                               MIT LICENSE                                   *
*******************************************************************************
Copyright 2019 Nicholas Walton

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*******************************************************************************
*/

#include "kero_platform.h"
#include "kero_sprite.h"
#include "kero_maze.h"
#include "kero_math.h"

ksprite_t frame_buffer;
bool draw_mode = true;
bool drawn_mode = false;
bool generate_maze = true;
int max = 0;

void HSVToRGB(float h, float s, float v, int* r, int* g, int* b) {
    h = Absolute(h/60.f);
    float c = v * s;
    float x = c * (1.f - Absolute(FMod(h, 2) - 1.f));
    c *= 255;
    x *= 255;
    int hue_prime = (int)(h)%6;
    switch(hue_prime) {
        case 0:{
            *r = c, *g = x, *b = 0;
        }break;
        case 1:{
            *r = x, *g = c, *b = 0;
        }break;
        case 2:{
            *r = 0, *g = c, *b = x;
        }break;
        case 3:{
            *r = 0, *g = x, *b = c;
        }break;
        case 4:{
            *r = x, *g = 0, *b = c;
        }break;
        case 5:{
            *r = c, *g = 0, *b = x;
        }break;
    }
}

int main(int argc, char* argv[]) {
    KPInit(1024,1024, "Maze Generator");
    KPSetTargetFramerate(0);
    srand(time(0));
    
    frame_buffer.pixels = kp_frame_buffer.pixels;
    frame_buffer.w = kp_frame_buffer.w;
    frame_buffer.h = kp_frame_buffer.h;
    
    maze_t maze = {0};
    wall_t* walls = NULL;
    unsigned int* weights;
    
    KSSetAllPixels(&frame_buffer, 0xff888888);
    
    bool game_running = true;
    while(game_running) {
        while(KPEventsQueued()) {
            kp_event_t* e = KPNextEvent();
            switch(e->type) {
                case KPEVENT_KEY_PRESS:{
                    switch(e->key) {
                        case KEY_ESCAPE:{
                            exit(0);
                        }break;
                        case KEY_SPACE:{
                            draw_mode = !draw_mode;
                        }break;
                        case KEY_G:{
                            generate_maze = true;
                        }break;
                    }
                }break;
                case KPEVENT_RESIZE:{
                    frame_buffer.pixels = kp_frame_buffer.pixels;
                    frame_buffer.w = kp_frame_buffer.w;
                    frame_buffer.h = kp_frame_buffer.h;
                }break;
                case KPEVENT_QUIT:{
                    exit(0);
                }break;
            }
            KPFreeEvent(e);
        }
        
        if(generate_maze) {
            KSSetAllPixels(&frame_buffer, 0xff888888);
            MazeGeneratePersistentWalk(&maze, &walls, 64,64, 16, 2, &frame_buffer);
            max = MazeFindSolution(&maze, &weights);
            generate_maze = false;
            drawn_mode = !draw_mode;
        }
        
        if(drawn_mode != draw_mode) {
            drawn_mode = draw_mode;
            if(draw_mode) {
                for(int y = 0; y < maze.h; ++y) {
                    for(int x = 0; x < maze.w; ++x) {
                        int r, g, b;
                        HSVToRGB((float)weights[x+y*maze.w] / (float)max * 360, 1, 1, &r, &g, &b);
                        KSDrawRectFilled(&frame_buffer, x*maze.cell_size, y*maze.cell_size,(x+1)*maze.cell_size, (y+1)*maze.cell_size, b + (g<<8) + (r<<16) + (255<<24));
                    }
                }
                
                int x = maze.end.x;
                int y = maze.end.y;
                while(x != maze.start.x || y != maze.start.y) {
                    KSDrawRectFilled(&frame_buffer, x*maze.cell_size+maze.cell_size/4.f, y*maze.cell_size+maze.cell_size/4.f,  x*maze.cell_size+3.f*maze.cell_size/4.f, y*maze.cell_size+3.f*maze.cell_size/4.f, 0xff888888);
                    int current_weight = weights[x+y*maze.w];
                    if(x > 0 && weights[(x-1)+y*maze.w] == current_weight-1 && maze.cells[x+y*maze.w] & MAZE_LEFT) {
                        --x;
                    }
                    else if(x < maze.w-1 && weights[(x+1)+y*maze.w] == current_weight-1 && maze.cells[x+y*maze.w] & MAZE_RIGHT) {
                        ++x;
                    }
                    else if(y > 0 && weights[x+(y-1)*maze.w] == current_weight-1 && maze.cells[x+y*maze.w] & MAZE_DOWN) {
                        --y;
                    }
                    else if(y < maze.h-1 && weights[x+(y+1)*maze.w] == current_weight-1 && maze.cells[x+y*maze.w] & MAZE_UP) {
                        ++y;
                    }
                }
                
                KSDrawRectFilled(&frame_buffer, maze.start.x*maze.cell_size + maze.cell_size/4.f, maze.start.y*maze.cell_size + maze.cell_size/4.f, maze.start.x*maze.cell_size + 3.f*maze.cell_size/4.f, maze.start.y*maze.cell_size + 3.f*maze.cell_size/4.f, 0xffffffff);
                KSDrawRectFilled(&frame_buffer, maze.end.x*maze.cell_size + maze.cell_size/4.f, maze.end.y*maze.cell_size + maze.cell_size/4.f, maze.end.x*maze.cell_size + 3.f*maze.cell_size/4.f, maze.end.y*maze.cell_size + 3.f*maze.cell_size/4.f, 0xff000000);
                
                for(int i = 0; i < sb_count(walls); ++i) {
                    KSDrawLine(&frame_buffer, walls[i].a.x, walls[i].a.y, walls[i].b.x, walls[i].b.y, 0xff000000);
                }
            }
            else {
                for(int y = 0; y < maze.h; ++y) {
                    for(int x = 0; x < maze.w; ++x) {
                        MazeDrawCell(&maze, x, y, &frame_buffer, 0xffffffff);
                    }
                }
            }
        }
        
        KPFlip();
    }
    
    return 0;
}