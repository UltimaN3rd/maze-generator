# Maze Generator

Generates mazes, chiefly with my custom "Persistent Walk" algorithm but also has functions for other maze algorithms.

You can use kero_maze_standalone.h for the maze generation functions with no dependency beyond the C standard library. kero_maze.h has extended functionality with drawing during generation, and wall creation (vec2 a, b representing each wall in the maze). I suggest you either start with kero_maze_standalone and build more functionality if desired by looking at kero_maze, or start with kero_maze, trim the parts you don't want and edit the code to use your libraries.

Persistent Walk produces mazes of different textures based on the "persistence" value it is given.

Low persistence:
![Low persistence](low_persistence.png)

High persistence:
![High persistence](high_persistence.png)
